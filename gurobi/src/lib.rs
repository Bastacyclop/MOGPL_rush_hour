#[link(name = "gurobi65")] extern "C" {}

pub mod model;
pub mod expression;
pub mod attributes;
pub mod parameters;

pub use model::Model;
pub use expression::{VariableType, ConstraintSense, infinity};

use std::{ptr, mem};
use std::os::raw::*;
use std::rc::Rc;
use parameters::Parameter;

pub struct Env {
    #[doc(hidden)]
    pub ptr: *mut ffi::Env, 
}

impl Env {
    pub fn new() -> Rc<Self> {
        let mut ptr = ptr::null_mut();
        let r = unsafe { ffi::GRBloadenv(&mut ptr, ptr::null()) };
        if r != Error::None { panic!("could not load environment"); }
        Rc::new(Env { ptr: ptr })
    }

    fn check(&self, e: Error, msg: &'static str) {
        use std::ffi::CStr;

        if e != Error::None {
            let error = unsafe {
                CStr::from_ptr(ffi::GRBgeterrormsg(self.ptr))
            };
            panic!("{}: {:?} because {:?}", msg, error, e);
        }
    }

    pub fn get_parameter<P: Parameter>(&self) -> <P as Parameter>::Type {
        let mut p = unsafe { mem::uninitialized() };
        let r = unsafe {
            <<P as Parameter>::Type as parameters::Single>::get(
                self.ptr,
                <P as Parameter>::c_name().as_ptr() as *const c_char,
                &mut p)
        };
        self.check(r, "could not get parameter");
        <<P as Parameter>::Type as parameters::Single>::convert_from(p)
    }

    pub fn set_parameter<P: Parameter>(&mut self, value: <P as Parameter>::Type) {
        <<P as Parameter>::Type as parameters::Single>::convert_to(value, |p| {
            let r = unsafe {
                <<P as Parameter>::Type as parameters::Single>::set(
                    self.ptr,
                    <P as Parameter>::c_name().as_ptr() as *const c_char,
                    p)
            };
            self.check(r, "could not set parameter");
        });
    }
}

impl Drop for Env {
    fn drop(&mut self) {
        unsafe { ffi::GRBfreeenv(self.ptr); }
    }
}

#[repr(i32)]
#[derive(Copy, Clone, PartialEq, Debug)] 
pub enum Error {
    None =                     00000,
    OutOfMemory =              10001,
    NullArgument =             10002,
    InvalidArgument =          10003,
    UnknownAttribute =         10004,
    DataNotAvaible =           10005,
    IndexOutOfRange =          10006,
    UnknownParameter =         10007,
    ValueOutOfRange =          10008,
    NoLicence =                10009,
    SizeLimitExceeded =        10010,
    CallBack =                 10011,
    FileRead =                 10012,
    FileWrite =                10013,
    Numeric =                  10014,
    IISNotInfeasable =         10015,
    NotForMip =                10016,
    OptimizationInProgress =   10017,
    Duplicates =               10018,
    Nodefile =                 10019,
    QNotPsd =                  10020,
    QCPEqualityConstraint =    10021,
    Network =                  10022,
    JobRejected =              10023,
    NotSupported =             10024,
    Exceed2BNonZeros =         10025,
    InvalidPiecewiseObj =      10026,
    UpdateModeChange =         10027,
    NotInModel =               20001,
    FailedToCreateModel =      20002,
    Internal =                 20003,
}

pub mod ffi {
    use std::os::raw::*;
    use super::Error;

    pub enum Env {}

    extern "C" {
        pub fn GRBloadenv(env: *mut *mut Env, log_path: *const c_char) -> Error;
        pub fn GRBfreeenv(env: *mut Env);
        pub fn GRBgeterrormsg(env: *mut Env) -> *mut c_char;
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn env() {
        let env = Env::new();
        let _model = Model::new("model", env);
    }
}
