use std::os::raw::*;
use Error;

pub struct TimeLimit;
impl Parameter for TimeLimit {
    type Type = f64;
    fn c_name() -> &'static [u8] { b"TimeLimit\0" }
}

#[doc(hidden)]
pub trait Parameter {
    type Type: Single;
    fn c_name() -> &'static [u8];
}

#[doc(hidden)]
pub trait Single: Sized {
    type C: Sized;

    unsafe fn get(env: *mut ffi::Env,
                  name: *const c_char,
                  value: &mut Self::C) -> Error;

    unsafe fn set(env: *mut ffi::Env,
                  name: *const c_char,
                  value: Self::C) -> Error;

    fn convert_from(c: Self::C) -> Self;
    fn convert_to<F>(Self, F) where F: FnOnce(Self::C);
}

impl Single for f64 {
    type C = c_double;

    unsafe fn get(model: *mut ffi::Env,
                  name: *const c_char,
                  value: &mut Self::C) -> Error {
        ffi::GRBgetdblparam(model, name, value)
    }

    unsafe fn set(model: *mut ffi::Env,
                  name: *const c_char,
                  value: Self::C) -> Error {
        ffi::GRBsetdblparam(model, name, value)
    }

    fn convert_from(c: Self::C) -> Self { c }
    fn convert_to<F>(s: Self, f: F) where F: FnOnce(Self::C) { f(s) }
}

pub mod ffi {
    use std::os::raw::*;
    pub use ffi::Env;
    pub use Error;

    extern "C" {
        pub fn GRBgetdblparam(env: *mut Env,
                              paramname: *const c_char,
                              value: *mut c_double) -> Error;
        pub fn GRBsetdblparam(env: *mut Env,
                              paramname: *const c_char,
                              value: c_double) -> Error;
    }
}
