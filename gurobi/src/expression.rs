use Model;

#[repr(u8)]
pub enum VariableType {
    Continuous = b'C',
    Binary = b'B',
    Integer = b'I',
}

#[repr(u8)]
pub enum ConstraintSense {
    LessEqual = b'<',
    Equal = b'=',
    GreaterEqual = b'>',
}

pub fn infinity<T: Infinity>() -> T {
    T::infinity()
}

pub trait Infinity {
    fn infinity() -> Self;
}

impl Infinity for i32 {
    fn infinity() -> Self { 1e100 as i32 }
}

impl Infinity for f64 {
    fn infinity() -> Self { 1e100 }
}

pub struct Factory {
    indices: Vec<i32>,
    values: Vec<f64>,
}

impl Factory {
    pub fn with_capacity(c: usize) -> Factory {
        Factory {
            indices: Vec::with_capacity(c),
            values: Vec::with_capacity(c),
        }
    }

    pub fn create_variable<'a>(&'a mut self, name: &'a str)
                               -> VariableBuilder<'a> {
        self.clear_coefs();
        VariableBuilder {
            factory: self,
            name: name,
            weight: 0.,
            lower: 0.,
            upper: infinity(),
        }
    }

    pub fn create_constraint<'a>(&'a mut self, name: &'a str)
                                 -> ConstraintBuilder<'a> {
        self.clear_coefs();
        ConstraintBuilder {
            factory: self,
            name: name,
        }
    }

    fn push_coef(&mut self, i: i32, v: f64) {
        self.indices.push(i);
        self.values.push(v);
    }

    fn clear_coefs(&mut self) {
        self.indices.clear();
        self.values.clear();
    }
}

pub struct VariableBuilder<'a> {
    factory: &'a mut Factory,
    name: &'a str,
    weight: f64,
    lower: f64,
    upper: f64,
}

impl<'a> VariableBuilder<'a> {
    pub fn add_coef(mut self, i: i32, v: f64) -> Self {
        self.factory.push_coef(i, v);
        self
    }

    pub fn weight(self, w: f64) -> Self {
        VariableBuilder { weight: w, ..self }
    }

    pub fn lower(self, l: f64) -> Self {
        VariableBuilder { lower: l, ..self }
    }

    pub fn upper(self, u: f64) -> Self {
        VariableBuilder { upper: u, ..self }
    }

    pub fn build(self, vtype: VariableType, model: &mut Model) {
        model.add_variable(self.name,
                           &self.factory.indices,
                           &self.factory.values,
                           self.weight,
                           self.lower,
                           self.upper,
                           vtype);
    }
}

pub struct ConstraintBuilder<'a> {
    factory: &'a mut Factory,
    name: &'a str,
}

impl<'a> ConstraintBuilder<'a> {
    pub fn add_coef(mut self, i: i32, v: f64) -> Self {
        self.factory.push_coef(i, v);
        self
    }

    pub fn build(self, sense: ConstraintSense, rhs: f64, model: &mut Model) {
        model.add_constraint(self.name,
                             &self.factory.indices,
                             &self.factory.values,
                             sense,
                             rhs);
    }
}
