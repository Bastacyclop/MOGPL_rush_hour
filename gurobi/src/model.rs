use std::{ptr, mem};
use std::ffi::CString;
use std::os::raw::*;
use std::rc::Rc;
use {Env, VariableType, ConstraintSense};
use attributes::{self, Attribute};

pub struct Model {
    ptr: *mut ffi::Model,
    env: Rc<Env>,
}

impl Model {
    pub fn new(name: &str, env: Rc<Env>) -> Self {
        let mut ptr = ptr::null_mut();
        let name = CString::new(name).unwrap();
        let r = unsafe {
            ffi::GRBnewmodel(env.ptr,
                             &mut ptr,
                             name.as_ptr(),
                             0,
                             ptr::null_mut(),
                             ptr::null_mut(),
                             ptr::null_mut(),
                             ptr::null_mut(),
                             ptr::null())
        };
        env.check(r, "could not create model");
        Model {
            ptr: ptr,
            env: env,
        }
    }

    pub fn add_variable(&mut self,
                        name: &str,
                        indices: &[i32],
                        values: &[f64],
                        weight: f64,
                        lower: f64,
                        upper: f64,
                        vtype: VariableType) {
        debug_assert!(indices.len() == values.len());

        let name = CString::new(name).unwrap();
        let r = unsafe {
            ffi::GRBaddvar(self.ptr,
                           indices.len() as c_int,
                           indices.as_ptr() as *mut _,
                           values.as_ptr() as *mut _,
                           weight,
                           lower,
                           upper,
                           vtype as c_char,
                           name.as_ptr())
        };
        self.env.check(r, "could not add variable");
    }

    pub fn add_constraint(&mut self,
                          name: &str,
                          indices: &[i32],
                          values: &[f64],
                          sense: ConstraintSense,
                          rhs: f64) {
        debug_assert!(indices.len() == values.len());

        let name = CString::new(name).unwrap();
        let r = unsafe {
            ffi::GRBaddconstr(self.ptr,
                              indices.len() as c_int,
                              indices.as_ptr() as *mut _,
                              values.as_ptr() as *mut _,
                              sense as c_char,
                              rhs,
                              name.as_ptr())
        };
        self.env.check(r, "could not add constraint");
    }

    pub fn optimize(&mut self, sense: Sense) -> Status {
        self.set_attribute::<attributes::ModelSense>(sense as i32);

        let r = unsafe { ffi::GRBoptimize(self.ptr) };
        self.env.check(r, "could not optimize model");

        unsafe { mem::transmute(self.get_attribute::<attributes::Status>()) }
    }

    pub fn update(&mut self) {
        let r = unsafe { ffi::GRBupdatemodel(self.ptr) };
        self.env.check(r, "could not update model");
    }

    pub fn get_attribute<A>(&self) -> <A as Attribute>::Type
        where A: Attribute,
              <A as Attribute>::Type: attributes::Single
    {
        let mut a = unsafe { mem::uninitialized() };
        let r = unsafe {
            <<A as Attribute>::Type as attributes::Single>::get(
                self.ptr,
                <A as Attribute>::c_name().as_ptr() as *const c_char,
                &mut a)
        };
        self.env.check(r, "could not get attribute");
        <<A as Attribute>::Type as attributes::Single>::convert_from(a)
    }

    pub fn set_attribute<A>(&mut self, value: <A as Attribute>::Type)
        where A: Attribute,
              <A as Attribute>::Type: attributes::Single
    {
        <<A as Attribute>::Type as attributes::Single>::convert_to(value, |a| { 
            let r = unsafe {
                <<A as Attribute>::Type as attributes::Single>::set(
                    self.ptr,
                    <A as Attribute>::c_name().as_ptr() as *const c_char,
                    a)
            };
            self.env.check(r, "could not set attribute");
        });
    }

    pub fn get_attributes<A>(&self,
                             start: usize,
                             len: usize,
                             output: &mut Vec<<<A as Attribute>::Type
                                               as attributes::Array>::Element>)
        where A: Attribute,
              <A as Attribute>::Type: attributes::Array
    {
        output.clear();
        output.resize(len, <<A as Attribute>::Type
                            as attributes::Array>::Element::default());
        let r = unsafe {
            <<A as Attribute>::Type as attributes::Array>::get_array(
                self.ptr,
                <A as Attribute>::c_name().as_ptr() as *const c_char,
                start as c_int,
                len as c_int,
                output.as_mut_ptr())
        };
        self.env.check(r, "could not get attributes");
    }
}

impl Drop for Model {
    fn drop(&mut self) {
        let r = unsafe { ffi::GRBfreemodel(self.ptr) };
        self.env.check(r, "could not drop model");
    }
}

#[derive(Debug)]
#[repr(i32)]
pub enum Sense {
    Minimize = 1,
    Maximize = -1,
}

#[derive(Debug)]
#[repr(i32)]
pub enum Status {
    Loaded         = 1,
    Optimal        = 2,
    Infeasible     = 3,
    InfOrBnd       = 4,
    Unbounded      = 5,
    Cutoff         = 6,
    IterationLimit = 7,
    NodeLimit      = 8,
    TimeLimit      = 9,
    SolutionLimit  = 10,
    Interrupted    = 11,
    Numeric        = 12,
    Suboptimal     = 13,
    InProgress     = 14,
}

pub mod ffi {
    use std::os::raw::*;
    use ffi::Env;
    use Error;

    pub enum Model {}

    extern "C" {
        pub fn GRBnewmodel(env: *mut Env,
                           model: *mut *mut Model,
                           name: *const c_char,
                           numvars: c_int,
                           obj: *mut c_double,
                           lb: *mut c_double,
                           ub: *mut c_double,
                           vtype: *mut c_char,
                           varnames: *const *const c_char) -> Error;
        pub fn GRBfreemodel(model: *mut Model) -> Error;
        pub fn GRBaddvar(model: *mut Model,
                         numnz: c_int,
                         vind: *mut c_int,
                         vval: *mut c_double,
                         obj: c_double,
                         lb: c_double,
                         ub: c_double,
                         vtype: c_char,
                         varname: *const c_char) -> Error;
        pub fn GRBaddconstr(model: *mut Model,
                            numnz: c_int,
                            cind: *mut c_int,
                            cval: *mut c_double,
                            sense: c_char,
                            rhs: c_double,
                            constrname: *const c_char) -> Error;
        pub fn GRBoptimize(model: *mut Model) -> Error;
        pub fn GRBupdatemodel(model: *mut Model) -> Error;
    }
}
