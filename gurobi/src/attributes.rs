use std::os::raw::*;
use std::ffi::{CStr, CString};
use Error;

pub struct ModelSense;
impl Attribute for ModelSense {
    type Type = i32;
    fn c_name() -> &'static [u8] { b"ModelSense\0" }
}

pub struct Status;
impl Attribute for Status {
    type Type = i32;
    fn c_name() -> &'static [u8] { b"Status\0" }
}

pub struct ObjectiveValue;
impl Attribute for ObjectiveValue {
    type Type = f64;
    fn c_name() -> &'static [u8] { b"ObjVal\0" }
}

pub struct ObjectiveBound;
impl Attribute for ObjectiveBound {
    type Type = f64;
    fn c_name() -> &'static [u8] { b"ObjBound\0" }
}

pub struct SolutionCount;
impl Attribute for SolutionCount {
    type Type = i32;
    fn c_name() -> &'static [u8] { b"SolCount\0" }
}

pub struct X;
impl Attribute for X {
    type Type = [f64];
    fn c_name() -> &'static [u8] { b"X\0" }
}

#[doc(hidden)]
pub trait Attribute {
    type Type: ?Sized;
    fn c_name() -> &'static [u8];
}

#[doc(hidden)]
pub trait Single: Sized {
    type C: Sized;

    unsafe fn get(model: *mut ffi::Model,
                  name: *const c_char,
                  value: &mut Self::C) -> Error;

    unsafe fn set(model: *mut ffi::Model,
                  name: *const c_char,
                  value: Self::C) -> Error;

    fn convert_from(c: Self::C) -> Self;
    fn convert_to<F>(Self, F) where F: FnOnce(Self::C);
}

#[doc(hidden)]
pub trait Array {
    type Element: Sized + Copy + Default;
    unsafe fn get_array(model: *mut ffi::Model,
                        name: *const c_char,
                        start: c_int,
                        len: c_int,
                        values: *mut Self::Element) -> Error;
}

impl Single for i32 {
    type C = c_int;

    unsafe fn get(model: *mut ffi::Model,
                  name: *const c_char,
                  value: &mut Self::C) -> Error {
        ffi::GRBgetintattr(model, name, value)
    }

    unsafe fn set(model: *mut ffi::Model,
                  name: *const c_char,
                  value: Self::C) -> Error {
        ffi::GRBsetintattr(model, name, value)
    }

    fn convert_from(c: Self::C) -> Self { c }
    fn convert_to<F>(s: Self, f: F) where F: FnOnce(Self::C) { f(s) }
}

impl Array for [i32] {
    type Element = i32;
    unsafe fn get_array(model: *mut ffi::Model,
                        name: *const c_char,
                        start: c_int,
                        len: c_int,
                        values: *mut Self::Element) -> Error {
        ffi::GRBgetintattrarray(model, name, start, len, values)
    }
}

impl Single for f64 {
    type C = c_double;

    unsafe fn get(model: *mut ffi::Model,
                  name: *const c_char,
                  value: &mut Self::C) -> Error {
        ffi::GRBgetdblattr(model, name, value)
    }

    unsafe fn set(model: *mut ffi::Model,
                  name: *const c_char,
                  value: Self::C) -> Error {
        ffi::GRBsetdblattr(model, name, value)
    }

    fn convert_from(c: Self::C) -> Self { c }
    fn convert_to<F>(s: Self, f: F) where F: FnOnce(Self::C) { f(s) }
}

impl Array for [f64] {
    type Element = f64;
    unsafe fn get_array(model: *mut ffi::Model,
                        name: *const c_char,
                        start: c_int,
                        len: c_int,
                        values: *mut Self::Element) -> Error {
        ffi::GRBgetdblattrarray(model, name, start, len, values)
    }
}

impl<'a> Single for &'a str {
    type C = *mut c_char;

    unsafe fn get(model: *mut ffi::Model,
                  name: *const c_char,
                  value: &mut Self::C) -> Error {
        ffi::GRBgetstrattr(model, name, value)
    }

    unsafe fn set(model: *mut ffi::Model,
                  name: *const c_char,
                  value: Self::C) -> Error {
        ffi::GRBsetstrattr(model, name, value)
    }

    fn convert_from(c: Self::C) -> Self {
        unsafe { CStr::from_ptr(c).to_str().unwrap() }
    }

    fn convert_to<F>(s: Self, f: F) where F: FnOnce(Self::C) {
        f(CString::new(s).unwrap().as_ptr() as *mut _)
    }
}

pub mod ffi {
    use std::os::raw::*;
    pub use model::ffi::Model;
    pub use Error;

    extern "C" {
        pub fn GRBgetintattr(model: *mut Model,
                             attrname: *const c_char,
                             value: *mut c_int) -> Error;
        pub fn GRBgetdblattr(model: *mut Model,
                             attrname: *const c_char,
                             value: *mut c_double) -> Error;
        pub fn GRBgetstrattr(model: *mut Model,
                             attrname: *const c_char,
                             value: *mut *mut c_char) -> Error;
        pub fn GRBgetintattrarray(model: *mut Model,
                                  attrname: *const c_char,
                                  start: c_int,
                                  len: c_int,
                                  values: *mut c_int) -> Error;
        pub fn GRBgetdblattrarray(model: *mut Model,
                                  attrname: *const c_char,
                                  start: c_int,
                                  len: c_int,
                                  values: *mut c_double) -> Error;
        pub fn GRBgetstrattrarray(model: *mut Model,
                                  attrname: *const c_char,
                                  start: c_int,
                                  len: c_int,
                                  values: *mut *mut c_char) -> Error;
        pub fn GRBsetintattr(model: *mut Model,
                             attrname: *const c_char,
                             value: c_int) -> Error;
        pub fn GRBsetdblattr(model: *mut Model,
                             attrname: *const c_char,
                             value: c_double) -> Error;
        pub fn GRBsetstrattr(model: *mut Model,
                             attrname: *const c_char,
                             value: *mut c_char) -> Error;
    }
}
