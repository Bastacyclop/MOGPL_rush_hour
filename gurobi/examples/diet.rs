extern crate gurobi as grb;

fn main() {
    // (name, min_nutrition, max_nutrition)
    let categories = [("calories", 1800., 2200.), 
                      ("protein" , 91.  , grb::infinity()),
                      ("fat"     , 0.   , 65.),
                      ("sodium"  , 0.   , 1779.)];

    // (name, cost, nutrition_values)
    let foods = [("hamburger", 2.49, [410., 24., 26., 730.]),
                 ("chicken"  , 2.89, [420., 32., 10., 1190.]),
                 ("hot dog"  , 1.50, [560., 20., 32., 1800.]),
                 ("fries"    , 1.89, [380.,  4., 19., 270.]),
                 ("macaroni" , 2.09, [320., 12., 10., 930.]),
                 ("pizza"    , 1.99, [320., 15., 12., 820.]),
                 ("salad"    , 2.49, [320., 31., 12., 1230.]),
                 ("milk"     , 0.89, [100.,  8., 2.5, 125.]),
                 ("ice cream", 1.59, [330.,  8., 10., 180.])];

    let mut m = &mut grb::Model::new("diet", grb::Env::new());
    let constr_nz = foods.len() + 1; 
    let mut f = grb::expression::Factory::with_capacity(constr_nz);

    for &(name, cost, _) in &foods {
        f.create_variable(name)
            .weight(cost)
            .build(grb::VariableType::Continuous, m);
    }

    for &(name, min, max) in &categories {
        f.create_variable(name)
            .lower(min)
            .upper(max)
            .build(grb::VariableType::Continuous, m);
    }

    m.update();

    for (c, &(name, _, _)) in categories.iter().enumerate() {
        let mut cb = f.create_constraint(name);
        for (f, &(_, _, values)) in foods.iter().enumerate() {
            cb = cb.add_coef(f as i32, values[c]);
        }
        cb.add_coef((foods.len() + c) as i32, -1.)
            .build(grb::ConstraintSense::Equal, 0., m);
    }

    use grb::model::Status;
    match m.optimize(grb::model::Sense::Minimize) {
        Status::Optimal => println!("Optimum"),
        _ => println!("NoSolution!"),
    }
}
