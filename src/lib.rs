extern crate gurobi;
#[macro_use]
extern crate log;
extern crate glutin;
extern crate lazybox_graphics as graphics;
extern crate lazybox_frameclock as frameclock;
#[macro_use]
extern crate conrod;

pub mod data;
pub mod linear_prog;
pub mod dijkstra;
pub mod gui;
