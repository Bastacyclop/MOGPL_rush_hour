extern crate rush_hour;
#[macro_use]
extern crate log;
extern crate flexi_logger;

use std::env;
use rush_hour::{data, linear_prog, dijkstra};

fn main() {
    let c = flexi_logger::LogConfig::new();
    flexi_logger::init(c, None).unwrap();

    let mut args = env::args();
    args.next();
    let puzzle_path = args.next().expect("missing puzzle path");
    let method = args.next().unwrap_or("gurobi".to_string());
    let objective = args.next().unwrap_or("RHM".to_string());
    let max_moves = args.next()
        .map(|a| a.parse().expect("N should be an integer"))
        .unwrap_or(50);

    let grid = data::Grid::from_file(puzzle_path).unwrap();

    println!("feasible configurations: {}",
             dijkstra::count_feasible_configurations(&grid));

    let solution = match &method as &str {
        "gurobi" => linear_prog::solve(&grid, max_moves, &objective),
        "dijkstra" => dijkstra::solve(&grid, max_moves, &objective),
        _ => panic!("unknown method")
    };

    let print_solution = |s: data::Solution| {
        println!("cost: {}", s.cost);
        println!("moves:");
        for m in s.moves {
            let i = m.vehicle as data::Size;
            if i == 0 {
                print!(" g");
            } else if i > grid.cars {
                print!("t{}", i - grid.cars);
            } else {
                print!("c{}", i);
            }
            println!(" to ({}, {})", m.target[0] + 1, m.target[1] + 1);
        }
    };

    use rush_hour::data::SolveResult::*;
    match solution {
        Optimal(s) => {
            println!("optimal solution found");
            print_solution(s);
        }
        Infeasible => {
            println!("infeasible");
        }
        TimeLimit(s_opt) => {
            println!("time limit reached");
            if let Some(s) = s_opt {
                print_solution(s);
            } else {
                println!("no solution found");
            }
        }
    }
}
