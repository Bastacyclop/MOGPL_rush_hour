extern crate rush_hour;
#[macro_use]
extern crate log;
extern crate flexi_logger;

use std::env;

fn main() {
    let c = flexi_logger::LogConfig::new();
    flexi_logger::init(c, None).unwrap();

    let mut args = env::args();
    args.next();
    rush_hour::gui::run("Rush Hour", args.next());
}
