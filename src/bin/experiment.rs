extern crate rush_hour;
extern crate time;

use std::{io, fs};
use std::path::Path;
use std::io::prelude::*;

use rush_hour::{linear_prog, dijkstra};
use rush_hour::data::*;

#[derive(Debug, Default, Clone)]
struct Result {
    rhm_time: u64,
    rhm_cost: Option<u32>,
    rhc_time: u64,
    rhc_cost: Option<u32>,
}

#[derive(Debug)]
struct PuzzleResult {
    linear_prog: Result,
    dijkstra: Result,
    feasible_configurations: usize,
}

const CATEGORIES: [(&'static str, usize); 4] = [
    ("débutant", 14),
    ("intermédiaire", 25),
    ("avancé", 31),
    ("expert", 50),
];

fn measure<F: FnOnce()>(f: F) -> u64 {
    let marker = time::precise_time_ns();
    f();
    time::precise_time_ns() - marker
}

fn solve<F>(grid: &Grid, max_moves: usize, mut f: F) -> Result
    where F: FnMut(&Grid, usize, &str) -> SolveResult
{
    let mut r = Result::default();
    let cost = |r: SolveResult| {
        use rush_hour::data::SolveResult::*;
        match r {
            Optimal(s) => Some(s.cost),
            Infeasible => None,
            TimeLimit(s_opt) => s_opt.map(|s| s.cost),
        }
    };
    r.rhm_time = measure(|| r.rhm_cost = cost(f(grid, max_moves, "RHM")));
    r.rhc_time = measure(|| r.rhc_cost = cost(f(grid, max_moves, "RHC")));
    r
}

fn process(path: &Path, max_moves: usize) -> PuzzleResult {
    let grid = Grid::from_file(path).unwrap();
    PuzzleResult {
        linear_prog: solve(&grid, max_moves, linear_prog::solve),
        dijkstra: solve(&grid, max_moves, dijkstra::solve),
        feasible_configurations:
            dijkstra::count_feasible_configurations(&grid),
    }
}

fn main() {
    let mut results = Vec::new();
    for &(name, max_moves) in &CATEGORIES {
        for entry in fs::read_dir("puzzles/".to_string() + name).unwrap() {
            let path = entry.unwrap().path();
            if path.is_file() {
                println!("processing {:?}", path);
                let result = process(&path, max_moves);
                results.push((path, result));
            }
        }
    }

    let f = fs::File::create("experiment/results").unwrap();
    let mut writer = io::BufWriter::new(f);
    writeln!(writer, "# g_rhm_c g_rhm_t g_rhc_c g_rhc_t d_rhm_c d_rhm_t d_rhc_d d_rhc_t feasible_configs")
        .unwrap();
    
    println!("{:^8} | {:^35} | {:^35} | {:10}",
             "", "gurobi", "dijkstra", "");
    println!("{0:^8} | {1:^16} | {2:^16} | {1:^16} | {2:^16} | {3:^10}",
             "", "rhm", "rhc", "");
    println!("{0:^8} | {1:^5} | {2:^8} | {1:^5} | {2:^8} | {1:^5} | {2:^8} | {1:^5} | {2:^8} | {3:^10}",
             "path", "cost", "time", "feasible");
    let print_separator = || {
        for _ in 0..(8+3+35+3+35+3+12) { print!("-"); }
        println!("");
    };
    let print_result = |r: &Result| {
        print_cost(r.rhm_cost);
        print!(" | ");
        print_time(r.rhm_time);
        print!(" | ");
        print_cost(r.rhc_cost);
        print!(" | ");
        print_time(r.rhc_time);
    };

    print_separator();
    for (file, result) in results {
        print!("{:<8} | ", file.file_stem().unwrap().to_str().unwrap());
        print_result(&result.linear_prog);
        print!(" | ");
        print_result(&result.dijkstra);
        println!(" | {:^12}", result.feasible_configurations);

        write_result(&mut writer, &result.linear_prog).unwrap();
        write!(writer, " ").unwrap();
        write_result(&mut writer, &result.dijkstra).unwrap();
        writeln!(writer, " {}", result.feasible_configurations).unwrap();
    }
    print_separator();
}

fn print_cost(c: Option<u32>) {
    if let Some(c) = c {
        print!("{:>5}", c);
    } else {
        print!(" none");
    }
}

fn write_cost<W: Write>(w: &mut W, c: Option<u32>) -> io::Result<()> {
    if let Some(c) = c {
        write!(w, "{}", c)
    } else {
        write!(w, "none")
    }
}

fn write_result<W: Write>(w: &mut W, r: &Result) -> io::Result<()> {
    try!(write_cost(w, r.rhm_cost));
    try!(write!(w, " {} ", r.rhm_time));
    try!(write_cost(w, r.rhc_cost));
    write!(w, " {}", r.rhc_time)
}

fn print_time(ns: u64) {
    let mut n = ns;
    for unit in &["ns", "µs", "ms", "s "] {
        if n < 100_000 {
            print!("{:>5} {}", n, unit);
            return;
        }
        n /= 1_000;
    }
    panic!("no suitable unit");
}
