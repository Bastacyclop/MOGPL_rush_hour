use std::rc::Rc;
use data::*;
use gurobi::*;
use gurobi::expression::Factory;

pub fn solve(grid: &Grid, max_moves: usize, objective: &str) -> SolveResult {
    let mut env = Env::new();
    Rc::get_mut(&mut env).unwrap().set_parameter::<parameters::TimeLimit>(120.0);
    let model = &mut Model::new("", env);
    let factory = &mut expression::Factory::with_capacity(16);

    // max_moves + 1 because we want to include max_moves
    let mut data = Data::new(max_moves + 1, grid);
    data.create_variables(objective, model, factory);
    model.update();
    data.create_constraints(model, factory);

    let status = model.optimize(model::Sense::Minimize);
    match status {
        model::Status::Optimal =>
            SolveResult::Optimal(extract_solution(model, &data)),
        model::Status::Infeasible => SolveResult::Infeasible,
        model::Status::TimeLimit => {
            let sc = model.get_attribute::<attributes::SolutionCount>();
            if sc > 0 {
                SolveResult::TimeLimit(Some(extract_solution(model, &data)))
            } else {
                SolveResult::TimeLimit(None)
            }
        }
        _ => panic!("unexpected model status: {:?}", status),
    }
}

fn extract_solution(model: &Model, data: &Data) -> Solution {
    let mut moves = Vec::new();
    let mut values = Vec::new();
    // TODO: smaller batches ?
    model.get_attributes::<attributes::X>(0, data.var_count(), &mut values);

    for k in 1..data.max_moves {
        for (i, vehicle) in data.grid.vehicles.iter().enumerate() {
            for (j_index, _) in possible_markers(vehicle, &data.grid).enumerate() {
                for (l_index, l) in possible_markers(vehicle, &data.grid).enumerate() {
                    let y = values[data.y_var(i, j_index, l_index, k) as usize];
                    if y > 0.0 {
                        moves.push(Move {
                            vehicle: i,
                            target: data.grid.cell_position(l),
                        });
                    }
                }
            }
        }
    }

    Solution {
        cost: model.get_attribute::<attributes::ObjectiveValue>() as u32,
        moves: moves,
    }
}

struct Data<'a> {
    grid: &'a Grid,
    max_moves: usize,
    vars_per_k: usize,
    i_offsets: Vec<usize>,
}

impl<'a> Data<'a> {
    fn new(max_moves: usize, grid: &'a Grid) -> Self {
        Data {
            grid: grid,
            max_moves: max_moves,
            vars_per_k: 0,
            i_offsets: Vec::with_capacity(grid.vehicles.len()),
        }
    }

    fn create_variables(&mut self,
                        objective: &str,
                        model: &mut Model,
                        factory: &mut Factory) {
        let mut offset = 0;
        for vehicle in &self.grid.vehicles {
            self.i_offsets.push(offset);
            offset += self.vars_per_ki(vehicle.length);
        }
        self.vars_per_k = offset;

        fn rhm_weight(_: usize) -> f64 { 1.0 }
        fn rhc_weight(moves: usize) -> f64 { moves as f64 }
        let weight = match objective {
            "RHM" => rhm_weight,
            "RHC" => rhc_weight,
            _ => panic!("unknown objective")
        };

        let mut o = 0;
        for k in 1..self.max_moves {
            for (i, vehicle) in self.grid.vehicles.iter().enumerate() {
                let mut vpki = 0;
                for (j_index, j) in possible_occupations(vehicle, &self.grid).enumerate() {
                    // z i, j, k
                    factory.create_variable("")
                        .build(VariableType::Binary, model);
                    debug_assert!(self.z_var(i, j_index, k) == o);
                    vpki += 1;
                    o += 1;

                    if is_possible_marker(j_index, vehicle, &self.grid) {
                        // x i, j, k
                        factory.create_variable("")
                            .build(VariableType::Binary, model);
                        debug_assert!(self.x_var(i, j_index, k) == o);
                        vpki += 1;
                        o += 1;
                        for (l_index, l) in possible_markers(vehicle, &self.grid).enumerate() {
                            let moves = interval(j, l, &self.grid).count() - 1;
                            // y i, j, l, k
                            factory.create_variable("")
                                .weight(weight(moves))
                                .build(VariableType::Binary, model);
                            debug_assert!(self.y_var(i, j_index, l_index, k) == o);
                            vpki += 1;
                            o += 1;
                        }
                    }
                }
                debug_assert!(vpki == self.vars_per_ki(vehicle.length));
            }
        }
    }

    fn ki_offset(&self, k: usize, i: usize) -> usize {
        self.vars_per_k * (k - 1) + self.i_offsets[i]
    }

    fn vars_per_ki(&self, length: Size) -> usize {
        let z_vars = self.grid.size as usize;
        let mcount = self.marker_count(length);
        let xy_vars = mcount * (1 + mcount);
        z_vars + xy_vars
    }

    fn marker_count(&self, length: Size) -> usize {
        (self.grid.size - (length - 1)) as usize
    }

    fn var_count(&self) -> usize {
        self.vars_per_k * (self.max_moves - 1)
    }

    fn j_offset(&self, i: usize, j_index: usize) -> usize {
        let length = self.grid.vehicles[i].length;
        let mcount = self.marker_count(length);
        if j_index < mcount {
            j_index * (2 + mcount)
        } else {
            mcount * (2 + mcount) + (j_index - mcount)
        }
    }

    fn z_var(&self, i: usize, j_index: usize, k: usize) -> i32 {
        (self.ki_offset(k, i) +
         self.j_offset(i, j_index)) as i32
    }

    fn x_var(&self, i: usize, j_index: usize, k: usize) -> i32 {
        (self.ki_offset(k, i) +
         self.j_offset(i, j_index) + 1) as i32
    }

    fn y_var(&self, i: usize, j_index: usize, l_index: usize, k: usize) -> i32 {
        (self.ki_offset(k, i) +
         self.j_offset(i, j_index) + 2 + l_index) as i32
    }

    fn create_constraints(&mut self, model: &mut Model, factory: &mut Factory) {
        self.create_constraint_1(model, factory);
        self.create_constraint_2(model, factory);
        self.create_constraint_3(model, factory);
        self.create_constraint_4(model, factory);
        self.create_constraint_5a(model, factory);
        self.create_constraint_5b(model, factory);
        self.create_constraint_5c(model, factory);
    }

    fn create_constraint_1(&self, model: &mut Model, factory: &mut Factory) {
        for k in 1..self.max_moves {
            for (i, vehicle) in self.grid.vehicles.iter().enumerate() {
                for (j_index, _) in possible_markers(vehicle, &self.grid).enumerate() {
                    let mut builder = factory.create_constraint("")
                        .add_coef(self.x_var(i, j_index, k), vehicle.length as f64);
                    for v in 0..vehicle.length {
                        let m_index = j_index + v as usize;
                        builder = builder.add_coef(self.z_var(i, m_index, k), -1.0);
                    }
                    builder.build(ConstraintSense::LessEqual, 0.0, model);
                }
            }
        }
    }

    fn create_constraint_2(&self, model: &mut Model, factory: &mut Factory) {
        for k in 1..self.max_moves {
            for (j, _) in self.grid.cells.iter().enumerate() {
                let mut builder = factory.create_constraint("");
                for (i, vehicle) in self.grid.vehicles.iter().enumerate() {
                    if let Some(j_index) = occupation_possible(vehicle, j, &self.grid) {
                        builder = builder.add_coef(self.z_var(i, j_index, k), 1.0);
                    }
                }
                builder.build(ConstraintSense::LessEqual, 1.0, model);
            }
        }
    }

    fn create_constraint_3(&self, model: &mut Model, factory: &mut Factory) {
        for k in 1..self.max_moves {
            for (i, vehicle) in self.grid.vehicles.iter().enumerate() {
                let mut builder = factory.create_constraint("");
                for (j_index, _) in possible_occupations(vehicle, &self.grid).enumerate() {
                    builder = builder.add_coef(self.z_var(i, j_index, k), 1.0);
                }
                builder.build(ConstraintSense::Equal, vehicle.length as f64, model);
            }
        }
    }

    fn create_constraint_4(&self, model: &mut Model, factory: &mut Factory) {
        for k in 1..self.max_moves {
            for (i, vehicle) in self.grid.vehicles.iter().enumerate() {
                for (j_index, j) in possible_markers(vehicle, &self.grid).enumerate() {
                    for (l_index, l) in possible_markers(vehicle, &self.grid).enumerate() {
                        for p in interval(j, l, &self.grid) {
                            let mut builder = factory.create_constraint("")
                                .add_coef(self.y_var(i, j_index, l_index, k), 1.0);
                            let mut rhs = 1.0;
                            if k > 1 {
                                for (i2, v2) in self.grid.vehicles.iter().enumerate() {
                                    if i != i2 {
                                        if let Some(j_index2) = intersection_possible(vehicle, v2, p, &self.grid) {
                                            builder = builder.add_coef(self.z_var(i2, j_index2, k - 1), 1.0);
                                        }
                                    }
                                }
                            } else { // k = 1, k - 1 = 0
                                if let Some(i2) = self.grid.vehicle_index(&self.grid.cells[p]) {
                                    if i != i2 { rhs -= 1.0; }
                                }
                            }
                            builder.build(ConstraintSense::LessEqual, rhs, model);
                        }
                    }
                }
            }
        }
    }

    fn create_constraint_5a(&self, model: &mut Model, factory: &mut Factory) {
        let i = self.grid.vehicle_index(&Cell::RedCar).unwrap();
        let j_index = (self.grid.size - 2) as usize;
        let k = self.max_moves - 1;
        factory.create_constraint("")
            .add_coef(self.x_var(i, j_index, k), 1.0)
            .build(ConstraintSense::Equal, 1.0, model);
    }

    fn create_constraint_5b(&self, model: &mut Model, factory: &mut Factory) {
        for k in 1..self.max_moves {
            let mut builder = factory.create_constraint("");
            for (i, vehicle) in self.grid.vehicles.iter().enumerate() {
                for (j_index, _) in possible_markers(vehicle, &self.grid).enumerate() {
                    for (l_index, _) in possible_markers(vehicle, &self.grid).enumerate() {
                        builder = builder.add_coef(self.y_var(i, j_index, l_index, k), 1.0);
                    }
                }
            }
            builder.build(ConstraintSense::LessEqual, 1.0, model);
        }
    }

    fn create_constraint_5c(&self, model: &mut Model, factory: &mut Factory) {
        for k in 1..self.max_moves {
            for (i, vehicle) in self.grid.vehicles.iter().enumerate() {
                for (j_index, j) in possible_markers(vehicle, &self.grid).enumerate() {
                    let mut builder = factory.create_constraint("");
                    let rhs = if k > 1 {
                        builder = builder.add_coef(self.x_var(i, j_index, k - 1), 1.0);
                        0.0
                    } else { // k = 1, k - 1 = 0
                        let j0 = self.grid.index(vehicle.marker[0], vehicle.marker[1]);
                        if j == j0 { -1.0 } else { 0.0 }
                    };
                    for (l_index, _) in possible_markers(vehicle, &self.grid).enumerate() {
                        builder = builder.add_coef(self.y_var(i, l_index, j_index, k), 1.0);
                    }
                    builder.add_coef(self.x_var(i, j_index, k), -1.0)
                        .build(ConstraintSense::GreaterEqual, rhs, model);
                }
            }
        }

        for k in 1..self.max_moves {
            for (i, vehicle) in self.grid.vehicles.iter().enumerate() {
                let mut builder = factory.create_constraint("");
                for (j_index, _) in possible_markers(vehicle, &self.grid).enumerate() {
                    builder = builder.add_coef(self.x_var(i, j_index, k), 1.0);
                }
                builder.build(ConstraintSense::Equal, 1.0, model);
            }
        }

        for k in 1..self.max_moves {
            for (i, vehicle) in self.grid.vehicles.iter().enumerate() {
                for (j_index, j) in possible_markers(vehicle, &self.grid).enumerate() {
                    for (l_index, _) in possible_markers(vehicle, &self.grid).enumerate() {
                        let builder = factory.create_constraint("")
                            .add_coef(self.y_var(i, j_index, l_index, k), 2.0)
                            .add_coef(self.x_var(i, l_index, k), -1.0);
                        if k > 1 {
                            builder.add_coef(self.x_var(i, j_index, k - 1), -1.0)
                                .build(ConstraintSense::LessEqual, 0.0, model);
                        } else {
                            let j0 = self.grid.index(vehicle.marker[0], vehicle.marker[1]);
                            let rhs = if j == j0 { 1.0 } else { 0.0 };
                            builder.build(ConstraintSense::LessEqual, rhs, model);
                        }
                    }
                }
            }
        }
    }
}

fn _possible_markers(v: &Vehicle, g: &Grid, length: Size) -> PositionIter {
    match v.orientation {
        Orientation::Horizontal => PositionIter {
            position: g.index(0, v.marker[1]),
            target: g.index(g.size - length, v.marker[1]),
            step: 1,
        },
        Orientation::Vertical => PositionIter {
            position: g.index(v.marker[0], 0),
            target: g.index(v.marker[0], g.size - length),
            step: g.size as isize,
        }
    }
}

fn possible_occupations(v: &Vehicle, g: &Grid) -> PositionIter {
    _possible_markers(v, g, 1)
}

fn possible_markers(v: &Vehicle, g: &Grid) -> PositionIter {
    _possible_markers(v, g, v.length)
}

fn is_possible_marker(j_index: usize, v: &Vehicle, g: &Grid) -> bool {
    (j_index as Size + v.length) <= g.size
}

fn interval(j: usize, l: usize, g: &Grid) -> PositionIter {
    let (j, l) = if j <= l { (j, l) } else { (l, j) };
    if (l - j) < g.size as usize {
        // horizontal
        PositionIter {
            position: j,
            target: l,
            step: 1,
        }
    } else {
        // vertical
        if cfg!(debug) {
            let jpos = g.cell_position(j);
            let lpos = g.cell_position(l);
            assert!(jpos[0] == lpos[0]);
        }
        PositionIter {
            position: j,
            target: l,
            step: g.size as isize,
        }
    }
}

fn intersection_possible(a: &Vehicle, b: &Vehicle, p: usize, g: &Grid) -> Option<usize> {
    let pos = g.cell_position(p);
    let (x, y) = (pos[0], pos[1]);
    let (ax, ay) = (a.marker[0], a.marker[1]);
    let (bx, by) = (b.marker[0], b.marker[1]);
    match (a.orientation, b.orientation) {
        (Orientation::Horizontal, Orientation::Horizontal) =>
            if ay == by { Some(x as usize) } else { None },
        (Orientation::Vertical, Orientation::Vertical) =>
            if ax == bx { Some(y as usize) } else { None },
        (Orientation::Horizontal, Orientation::Vertical) =>
            if ay == y && bx == x { Some(y as usize) } else { None },
        (Orientation::Vertical, Orientation::Horizontal) =>
            if ax == x && by == y { Some(x as usize) } else { None },
    }
}

fn occupation_possible(v: &Vehicle, p: usize, g: &Grid) -> Option<usize> {
    let pos = g.cell_position(p);
    let (x, y) = (pos[0], pos[1]);
    match v.orientation {
        Orientation::Horizontal =>
            if y == v.marker[1] { Some(x as usize) } else { None },
        Orientation::Vertical =>
            if x == v.marker[0] { Some(y as usize) } else { None },
    }
}

struct PositionIter {
    position: usize,
    target: usize,
    step: isize,
}

impl Iterator for PositionIter {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        if self.step == 0 {
            None
        } else {
            let p = self.position;
            if self.position == self.target {
                self.step = 0;
            }
            if self.step < 0 {
                self.position -= (-self.step) as usize;
            } else {
                self.position += self.step as usize;
            }
            Some(p)
        }
    }
}

#[cfg(test)]
pub mod test {
    use data::*;

    #[test]
    fn possible_occupations() {
        let g = &grid();
        assert!(super::possible_occupations(&g.vehicles[0], g).eq(4..8));
        assert!(super::possible_occupations(&g.vehicles[1], g).eq(0..4));
        assert!(super::possible_occupations(&g.vehicles[2], g)
                .eq([2, 6, 10, 14].iter().cloned()));
    }

    #[test]
    fn possible_markers() {
        let g = &grid();
        assert!(super::possible_markers(&g.vehicles[0], g).eq(4..7));
        assert!(super::possible_markers(&g.vehicles[1], g).eq(0..3));
        assert!(super::possible_markers(&g.vehicles[2], g)
                .eq([2, 6].iter().cloned()));
    }

    #[test]
    fn interval() {
        let g = &grid();
        assert!(super::interval(1, 3, g).eq(1..4));
        assert!(super::interval(5, 6, g).eq(5..7));
        assert!(super::interval(12, 14, g).eq(12..15));
        assert!(super::interval(0, 8, g).eq([0, 4, 8].iter().cloned()));
        assert!(super::interval(2, 6, g).eq([2, 6].iter().cloned()));
        assert!(super::interval(3, 15, g).eq([3, 7, 11, 15].iter().cloned()));
    }

    #[test]
    fn intersection_possible() {
        let g = &grid();
        let (r, c, t) = (&g.vehicles[0], &g.vehicles[1], &g.vehicles[2]);
        assert!(super::intersection_possible(r, c, 3, g) == None);
        assert!(super::intersection_possible(r, t, 5, g) == None);
        assert!(super::intersection_possible(r, t, 6, g) == Some(1));
        assert!(super::intersection_possible(t, r, 6, g) == Some(2));
        assert!(super::intersection_possible(c, t, 7, g) == None);
        assert!(super::intersection_possible(c, t, 2, g) == Some(0));
        assert!(super::intersection_possible(t, c, 2, g) == Some(2));
    }

    #[test]
    fn occupation_possible() {
        let g = &grid();
        assert!(super::occupation_possible(&g.vehicles[0], 0, g) == None);
        assert!(super::occupation_possible(&g.vehicles[0], 5, g) == Some(1));
        assert!(super::occupation_possible(&g.vehicles[1], 6, g) == None);
        assert!(super::occupation_possible(&g.vehicles[1], 3, g) == Some(3));
        assert!(super::occupation_possible(&g.vehicles[2], 9, g) == None);
        assert!(super::occupation_possible(&g.vehicles[2], 10, g) == Some(2));
    }

    pub fn grid() -> Grid {
        Grid {
            size: 4,
            cars: 1,
            trucks: 1,
            cells: vec![
                Cell::Empty, Cell::Car(1), Cell::Car(1), Cell::Empty,
                Cell::RedCar, Cell::RedCar, Cell::Truck(1), Cell::Empty,
                Cell::Empty, Cell::Empty, Cell::Truck(1), Cell::Empty,
                Cell::Empty, Cell::Empty, Cell::Truck(1), Cell::Empty,
            ],
            vehicles: vec![
                Vehicle {
                    marker: [0, 1],
                    orientation: Orientation::Horizontal,
                    length: 2,
                },
                Vehicle {
                    marker: [1, 0],
                    orientation: Orientation::Horizontal,
                    length: 2,
                },
                Vehicle {
                    marker: [2, 1],
                    orientation: Orientation::Vertical,
                    length: 3,
                }
            ]
        }
    }
}
