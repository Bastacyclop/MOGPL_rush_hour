use std::cmp;
use std::collections::{BinaryHeap, HashMap, HashSet};
use data::*;

struct Node {
    cells: Vec<Cell>,
    cost: u32,
    moves: u32, // RHM: cost = moves :/
    move_id: Option<usize>,
}

impl Node {
    fn is_goal(&self, grid: &Grid) -> bool {
        let gy = grid.vehicles[0].marker[1];
        match self.cells[grid.index(grid.size - 1, gy)] {
            Cell::RedCar => true,
            _ => false,
        }
    }
}

// we want the min in a max-heap
impl cmp::Ord for Node {
    fn cmp(&self, other: &Node) -> cmp::Ordering {
        other.cost.cmp(&self.cost)
    }
}

impl cmp::PartialOrd for Node {
    fn partial_cmp(&self, other: &Node) -> Option<cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl cmp::PartialEq for Node {
    fn eq(&self, other: &Node) -> bool {
        other.cost.eq(&self.cost)
    }
}

impl cmp::Eq for Node {}

struct MoveNode {
    m: Move,
    previous: Option<usize>,
}

fn rhm_cost(_: Size, _: Size) -> u32 { 1 }

fn rhc_cost(a: Size, b: Size) -> u32 {
    (if a > b { a - b } else { b - a }) as u32
}

pub fn solve(grid: &Grid, _max_moves: usize, objective: &str) -> SolveResult {
    let move_cost = match objective {
        "RHM" => rhm_cost,
        "RHC" => rhc_cost,
        _ => panic!("unknown objective")
    };

    let mut cost_so_far = HashMap::new();
    let mut heap = BinaryHeap::new();
    let mut move_nodes: Vec<MoveNode> = Vec::new();

    cost_so_far.insert(grid.cells.clone(), 0);
    heap.push(Node { cells: grid.cells.clone(),
                     cost: 0,
                     moves: 0,
                     move_id: None });

    let mut solution = None;
    while let Some(node) = heap.pop() {
        if node.is_goal(grid) {
            solution = Some(node);
            break;
        }

        let Node { cells, cost, moves, move_id } = node;
        /* not compatible with the algorithm
        if moves >= max_moves as u32 { continue; }
        */
        if cost_so_far.get(&cells).map(|&c| cost > c).unwrap_or(false) {
            continue;
        }

        let cost = |a, b| cost + move_cost(a, b);
        for_possible_moves(cells, cost, grid, |m, c, cells| {
            if cost_so_far.get(&cells).map(|&prev_c| c < prev_c)
                .unwrap_or(true)
            {
                let next_mid = move_nodes.len();
                move_nodes.push(MoveNode { m: m, previous: move_id });
                heap.push(Node { cells: cells.clone(),
                                 cost: c,
                                 moves: moves + 1,
                                 move_id: Some(next_mid) });
                cost_so_far.insert(cells, c);
            }
        });
    }

    println!("explored {} nodes", cost_so_far.len());
    if let Some(node) = solution {
        let mut solution_moves = Vec::new();
        let mut id_opt = node.move_id;
        while let Some(id) = id_opt {
            let mn = &move_nodes[id];
            solution_moves.push(mn.m.clone());
            id_opt = mn.previous;
        }
        solution_moves.reverse();
        SolveResult::Optimal(Solution {
            cost: node.cost,
            moves: solution_moves
        })
    } else {
        SolveResult::Infeasible
    }
}

pub fn count_feasible_configurations(grid: &Grid) -> usize {
    struct Node {
        moves: u32,
        cells: Vec<Cell>,
    }
    let mut visited = HashSet::new();
    let mut stack = Vec::new();

    stack.push(Node { moves: 0, cells: grid.cells.clone() });

    while let Some(Node { moves, cells }) = stack.pop() {
        for_possible_moves(cells, |_, _| 0, grid, |_, _, cells| {
            if !visited.contains(&cells) {
                stack.push(Node { moves: moves + 1, cells: cells.clone() });
                visited.insert(cells);
            }
        });
    }

    visited.len()
}

fn for_possible_moves<C, F>(cells: Vec<Cell>, cost: C, grid: &Grid, mut f: F)
    where C: Fn(Size, Size) -> u32, F: FnMut(Move, u32, Vec<Cell>)
{
    for (v, vehicle) in grid.vehicles.iter().enumerate() {
        let tail_len = vehicle.length - 1;
        match vehicle.orientation {
            Orientation::Horizontal => {
                let y = vehicle.marker[1];
                let mut ax = 0;
                let mut x = 0;
                let mut cell;
                loop {
                    cell = cells[grid.index(x, y)];
                    match grid.vehicle_index(&cell) {
                        Some(i) => if i == v { break; } else { ax = x + 1; },
                        None => {}
                    }
                    x += 1;
                }
                let mut possible_move = |i| {
                    let c = cost(i, x);
                    let mut cells = cells.clone();
                    for j in x..(x + vehicle.length) {
                        cells[grid.index(j, y)] = Cell::Empty;
                    }
                    for j in i..(i + vehicle.length) {
                        cells[grid.index(j, y)] = cell;
                    }
                    f(Move { vehicle: v, target: [i, y] }, c, cells);
                };
                for i in ax..x {
                    possible_move(i);
                }
                let mut bx = x + 1;
                while bx < (grid.size - tail_len) {
                    match cells[grid.index(bx + tail_len, y)] {
                        Cell::Empty => {
                            possible_move(bx);
                            bx += 1;
                        }
                        _ => break,
                    }
                }
            }
            Orientation::Vertical => {
                let x = vehicle.marker[0];
                let mut ay = 0;
                let mut y = 0;
                let mut cell;
                loop {
                    cell = cells[grid.index(x, y)];
                    match grid.vehicle_index(&cell) {
                        Some(i) => if i == v { break; } else { ay = y + 1; },
                        None => {}
                    }
                    y += 1;
                }
                let mut possible_move = |i| {
                    let c = cost(i, y);
                    let mut cells = cells.clone();
                    for j in y..(y + vehicle.length) {
                        cells[grid.index(x, j)] = Cell::Empty;
                    }
                    for j in i..(i + vehicle.length) {
                        cells[grid.index(x, j)] = cell;
                    }
                    f(Move { vehicle: v, target: [x, i] }, c, cells);
                };
                for i in ay..y {
                    possible_move(i);
                }
                let mut by = y + 1;
                while by < (grid.size - tail_len) {
                    match cells[grid.index(x, by + tail_len)] {
                        Cell::Empty => {
                            possible_move(by);
                            by += 1;
                        }
                        _ => break,
                    }
                }
            }
        }
    }
}

#[cfg(test)]
mod test {
    use data::*;
    
    #[test]
    fn for_possible_moves() {
        let g = &grid();
        let n = super::Node { cells: g.cells.clone(),
                              cost: 0,
                              moves: 0,
                              move_id: None };
        let expected = [
            (Move { vehicle: 1, target: [0, 0] }, 1, [
                Cell::Car(1), Cell::Car(1), Cell::Empty, Cell::Empty,
                Cell::RedCar, Cell::RedCar, Cell::Truck(1), Cell::Empty,
                Cell::Empty, Cell::Empty, Cell::Truck(1), Cell::Empty,
                Cell::Empty, Cell::Empty, Cell::Truck(1), Cell::Empty,
            ]),
            (Move { vehicle: 1, target: [2, 0] }, 1, [
                Cell::Empty, Cell::Empty, Cell::Car(1), Cell::Car(1),
                Cell::RedCar, Cell::RedCar, Cell::Truck(1), Cell::Empty,
                Cell::Empty, Cell::Empty, Cell::Truck(1), Cell::Empty,
                Cell::Empty, Cell::Empty, Cell::Truck(1), Cell::Empty,
            ]),
        ];
        let mut expected = expected.iter();
        super::for_possible_moves(n.cells, super::rhm_cost, g, |m, c, cells| {
            let &(ref em, ec, ref ecells) = expected.next().unwrap();
            assert_eq!(em, &m);
            assert_eq!(ec, c);
            assert!(ecells == &cells as &[Cell]);
        });
        assert!(expected.next().is_none());
    }

    fn grid() -> Grid {
        ::linear_prog::test::grid()
    }
}
