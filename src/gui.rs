use glutin::{Event, VirtualKeyCode};
use graphics::Graphics;
use graphics::combined::conrod::Renderer;
use frameclock::*;
use conrod::{self, widget};
use data::{self, Grid, Solution, SolveResult};
use {linear_prog, dijkstra};

const WIDTH: u32 = 860;
const HEIGHT: u32 = 640;

const METHODS: &'static [&'static str] = &["gurobi", "dijkstra"];
const METHOD_CALLS: [fn(&Grid, usize, &str) -> SolveResult; 2] = [
    linear_prog::solve,
    dijkstra::solve,
];
const OBJECTIVES: &'static [&'static str] = &["RHM", "RHC"];

struct Data {
    grid: Option<Grid>,
    colors: Vec<conrod::Color>,
    solutions: [[Option<SolveResult>; 2]; 2],
    selected_puzzle: Option<String>,
    selected_method: usize,
    selected_objective: usize,
    max_moves: usize,
}

const CLEAR_CACHE: [[Option<SolveResult>; 2]; 2] = [[None, None], [None, None]];

impl Data {
    fn new() -> Self {
        Data {
            grid: None,
            colors: Vec::new(),
            solutions: CLEAR_CACHE,
            max_moves: 50,
            selected_puzzle: None,
            selected_method: 0,
            selected_objective: 0,
        }
    }

    fn load_puzzle(&mut self) {
        if let Some(path) = self.selected_puzzle.take() {
            self.grid = data::Grid::from_file(path).ok();
            self.solutions = CLEAR_CACHE;
            self.colors.clear();

            if let &Some(ref g) = &self.grid {
                self.colors.push(conrod::Color::Rgba(0.9, 0.0, 0.0, 1.0));
                let d = (g.cars + g.trucks) as f32 - 1.0;

                for (i, _) in g.vehicles.iter().skip(1).enumerate() {
                    let ratio = i as f32 / d;
                    let (r, g, b) = if ratio < 1./3. {
                        (3.*ratio, 0.2, 0.1)
                    } else if ratio < 2./3. {
                        (0.1, 3.*ratio - 1., 0.2)
                    } else {
                        (0.2, 0.1, 3.*ratio - 2.)
                    };
                    self.colors.push(conrod::Color::Rgba(0.05 + 0.8*r,
                                                         0.05 + 0.9*g,
                                                         0.05 + 0.9*b,
                                                         1.0));
                }
            }
        }
    }

    fn optimize(&mut self) {
        if let &mut Data { grid: Some(ref g),
                           max_moves,
                           ref mut solutions,
                           selected_method: method,
                           selected_objective: objective, .. } = self {
            solutions[method][objective] = Some(
                METHOD_CALLS[method](g, max_moves, OBJECTIVES[objective]));
        }
    }

    fn solution_cache(&self) -> Option<&SolveResult> {
        self.solutions[self.selected_method][self.selected_objective].as_ref()
    }
}

const EMPTY_COLOR: conrod::Color = conrod::Color::Rgba(0.7, 0.7, 0.7, 1.0);
const BAR_COLOR: conrod::Color = conrod::color::DARK_CHARCOAL;

pub fn run(title: &'static str, puzzle_path: Option<String>) {
    use glutin::WindowBuilder;
    use conrod::UiBuilder;
    use conrod::backend::glutin::convert as event_to_conrod;

    let builder = WindowBuilder::new()
        .with_title(title)
        .with_dimensions(WIDTH, HEIGHT);

    let (window, mut graphics) = Graphics::new(builder);
    let mut renderer = Renderer::new(window.hidpi_factor(), &mut graphics);

    let mut ui = UiBuilder::new().build();
    ui.fonts.insert_from_file("fonts/NotoSans/NotoSans-Regular.ttf").unwrap();

    let mut ids = Ids::new(ui.widget_id_generator());
    let image_map = conrod::image::Map::new();

    let mut data = Data::new();
    puzzle_path.map(|p| data.selected_puzzle = Some(p));
    data.load_puzzle();

    let mut frameclock = FrameClock::start(1.);

    'main: loop {
        let (w, h) = window.get_inner_size_pixels().unwrap();
        let dpi_factor = window.hidpi_factor() as conrod::Scalar;
        let delta_time = frameclock.reset();

        for event in window.poll_events() {
            match event {
                Event::KeyboardInput(_, _, Some(VirtualKeyCode::Escape)) |
                Event::Closed => break 'main,
                _ => {},
            }

            let (w, h) = (w as conrod::Scalar, h as conrod::Scalar);
            if let Some(event) = event_to_conrod(event.clone(), w, h, dpi_factor) {
                ui.handle_event(event);
            }
        }
        
        ui.handle_event(conrod::event::render(delta_time, w, h, dpi_factor));
        set_widgets(&mut ui, &mut ids, &mut data);

        let mut frame = graphics.draw();
        ui.draw_if_changed()
            .map(|ps| renderer.changed(ps, &image_map, &mut frame));
        renderer.render(&mut frame);
        frame.present(&window);
    }
}

widget_ids! {
    struct Ids {
        canvas,
        select_puzzle,
        select_method,
        select_objective,
        select_max_moves,
        optimize,
        matrix,
        solve_result,
        cost,
        solution,
    }
}

fn set_widgets(ui: &mut conrod::Ui, ids: &mut Ids, data: &mut Data) {
    let mut ui = &mut ui.set_widgets();
    use conrod::{Positionable, Sizeable, Widget,
                 Borderable, Colorable, Labelable};

    widget::Canvas::new()
        .color(conrod::color::DARK_CHARCOAL)
        .border(0.)
        .set(ids.canvas, ui);

    let head_size = 40.;
    let grid_size = HEIGHT as f64 - head_size;
    let item_size = grid_size / 6.;
    let right_size = WIDTH as f64 - grid_size;

    let mut events = widget::TextBox::new(data.selected_puzzle.as_ref()
                                          .map(|s| s as &str)
                                          .unwrap_or("*enter puzzle path*"))
        .top_left_of(ids.canvas)
        .w_h(item_size * 3., head_size)
        .font_size(14)
        .pad_text(10.0)
        .border_color(BAR_COLOR)
        .set(ids.select_puzzle, ui);
    match events.pop() {
        Some(widget::text_box::Event::Update(path)) =>
            data.selected_puzzle = Some(path),
        Some(widget::text_box::Event::Enter) => data.load_puzzle(),
        None => {}
    }

    let sm_opt = widget::DropDownList::new(METHODS, Some(data.selected_method))
        .right_from(ids.select_puzzle, 0.0)
        .w_h(item_size, head_size)
        .border_color(BAR_COLOR)
        .set(ids.select_method, ui);
    sm_opt.map(|sm| data.selected_method = sm);

    let so_opt = widget::DropDownList::new(OBJECTIVES, Some(data.selected_objective))
        .right_from(ids.select_method, 0.0)
        .w_h(item_size, head_size)
        .border_color(BAR_COLOR)
        .set(ids.select_objective, ui);
    so_opt.map(|so| data.selected_objective = so);

    if let Some(max_moves) =
        widget::Slider::new(data.max_moves as f32, 0.0, 60.0)
        .right_from(ids.select_objective, 0.0)
        .w_h(item_size, head_size)
        .label(&format!("N = {:>2}", data.max_moves))
        .label_color(conrod::Color::Rgba(0.678, 0.498, 0.659, 1.0))
        .border_color(BAR_COLOR)
        .set(ids.select_max_moves, ui)
    {
        data.max_moves = max_moves as usize;
    }

    let mut optimize = widget::Toggle::new(true)
        .top_right_of(ids.canvas)
        .w_h(right_size, head_size)
        .label("OPTIMIZE")
        .border_color(BAR_COLOR)
        .color(conrod::Color::Rgba(0.937, 0.160, 0.160, 1.0))
        .set(ids.optimize, ui);
    if optimize.next().is_some() {
        data.optimize(); // TODO: don't freeze here
    }

    if let &Some(ref g) = &data.grid {
        let mut cells = widget::Matrix::new(g.size as usize, g.size as usize)
            .bottom_left_of(ids.canvas)
            .wh([grid_size; 2])
            .set(ids.matrix, ui);

        while let Some(cell) = cells.next(ui) {
            let x = cell.col as data::Size;
            let y = cell.row as data::Size;
            if x >= g.size || y >= g.size { continue; } // FIXME: nasty bug dodge
            let c = if let Some(i) = g.vehicle_index(&g.get(x, y)) {
                data.colors[i]
            } else {
                EMPTY_COLOR
            };
            let w = widget::BorderedRectangle::new([0.; 2]).color(c);
            cell.set(w, ui);
        }
    }

    let display_solution = |s: &Solution, ids: &mut Ids, ui: &mut conrod::UiCell| {
        widget::TextBox::new(&format!("cost: {}", s.cost))
            .down_from(ids.solve_result, 0.0)
            .w_h(right_size, head_size)
            .align_text_middle()
            .text_color(conrod::Color::Rgba(0.937, 0.160, 0.160, 1.0))
            .color(conrod::color::DARK_CHARCOAL)
            .set(ids.cost, ui);

        let moves = s.moves.len();
        let (mut items, scroll) = widget::List::new(moves, item_size / 3.)
            .scrollbar_next_to()
            .scrollbar_color(conrod::color::WHITE)
            .down_from(ids.cost, 0.0)
            .w_h(right_size, grid_size - 2.0*head_size)
            .set(ids.solution, ui);
        scroll.map(|s| s.set(ui));

        while let Some(item) = items.next(ui) {
            let m = &s.moves[item.i];
            let text = format!("move n°{:>2} - go to ({}, {})",
                               item.i + 1, m.target[0] + 1, m.target[1] + 1);
            let w = widget::TextBox::new(&text)
                .align_text_middle()
                .text_color(conrod::color::WHITE)
                .color(data.colors[m.vehicle]);
            item.set(w, ui);
        }
    };

    use data::SolveResult::*;
    match data.solution_cache() {
        Some(&Optimal(ref s)) => {
            widget::TextBox::new("Optimal")
                .down_from(ids.optimize, 0.0)
                .w_h(right_size, head_size)
                .align_text_middle()
                .text_color(conrod::Color::Rgba(0.988, 0.686, 0.243, 1.0))
                .color(conrod::color::DARK_CHARCOAL)
                .set(ids.solve_result, ui);

            display_solution(s, ids, ui);
        }
        Some(&Infeasible) => {
            widget::TextBox::new("Infeasible")
                .down_from(ids.optimize, 0.0)
                .w_h(right_size, grid_size)
                .align_text_middle()
                .text_color(conrod::Color::Rgba(0.988, 0.686, 0.243, 1.0))
                .color(conrod::color::DARK_CHARCOAL)
                .set(ids.solve_result, ui);
        }
        Some(&TimeLimit(ref s_opt)) => {
            widget::TextBox::new("Time limit reached")
                .down_from(ids.optimize, 0.0)
                .w_h(right_size, head_size)
                .align_text_middle()
                .text_color(conrod::Color::Rgba(0.988, 0.686, 0.243, 1.0))
                .color(conrod::color::DARK_CHARCOAL)
                .set(ids.solve_result, ui);

            s_opt.as_ref().map(|s| display_solution(s, ids, ui));
        }
        None => {}
    }
}
