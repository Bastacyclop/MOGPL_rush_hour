//! ## File format
//!
//! With `c` representing a grid cell:
//!
//! ```none
//! h w
//! c c c --
//! c c c  |
//! c c c -- h
//! |___|
//!     w
//! ```
//!
//! A cell being:
//!
//! - `0`: empty
//! - `g`: the red car
//! - `cN`: a car of id `N`
//! - `tN`: a truck of id `N`

use std::path::Path;
use std::fs::File;
use std::{io, str};
use std::io::prelude::*;

pub type Size = u16;
pub type Id = u16;

#[derive(Debug)]
pub struct Grid {
    pub size: Size,
    pub cars: Id,
    pub trucks: Id,
    pub cells: Vec<Cell>,
    pub vehicles: Vec<Vehicle>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Cell {
    Empty,
    RedCar,
    Car(Id),
    Truck(Id),
}

#[derive(Debug, Clone)]
pub struct Vehicle {
    pub marker: [Size; 2],
    pub orientation: Orientation,
    pub length: Size,
}

#[derive(Debug, Clone, Copy)]
pub enum Orientation {
    Horizontal,
    Vertical,
}

impl Grid {
    pub fn from_file<P: AsRef<Path>>(path: P) -> io::Result<Self> {
        let f = try!(File::open(path));
        let mut reader = io::BufReader::new(f);

        let mut line = String::new();
        try!(reader.read_line(&mut line));
        info!("{}", line.trim_right());
        let (w, h) = {
            let mut words = line.split_whitespace();
            let h = words.next().expect("expected grid height")
                .parse().expect("could not parse grid height");
            let w = words.next().expect("expected grid width")
                .parse().expect("could not parse grid width");
            if words.next().is_some() {
                warn!("extra data ignored");
            }
            (w, h)
        };
        assert_eq!(w, h);

        let mut cells = Vec::with_capacity(w as usize * h as usize);
        let mut cars = 0;
        let mut trucks = 0;
        for _ in 0..h {
            line.clear();
            try!(reader.read_line(&mut line));
            info!("{}", line.trim_right());
            let mut words = line.split_whitespace();
            for _ in 0..w {
                let c = Cell::from_str(
                    words.next().expect("expected cell"),
                    &mut cars,
                    &mut trucks);
                cells.push(c);
            }
            if words.next().is_some() {
                warn!("extra data ignored");
            }
        }

        if reader.bytes().next().is_some() {
            warn!("extra data ignored");
        }

        struct WipVehicle {
            marker: Option<[Size; 2]>,
            orientation: Option<Orientation>,
            length: Size,
        }
        let mut vehicles: Vec<_> = (0..(1 + cars + trucks))
            .map(|_| WipVehicle {
                marker: None,
                orientation: None,
                length: 0,
            }).collect();

        for y in 0..h {
            for x in 0..w {
                let c = &cells[x as usize + w as usize * y as usize];
                if let Some(i) = c.vehicle_index(cars) {
                    let v = &mut vehicles[i];
                    if let Some(ref mut m) = v.marker {
                        match v.orientation {
                            None => {
                                if m[1] == y {
                                    v.orientation = Some(Orientation::Horizontal);
                                } else {
                                    assert!(m[0] == x);
                                    v.orientation = Some(Orientation::Vertical);
                                }
                            }
                            Some(Orientation::Horizontal) => {
                                assert!(m[1] == y);
                            }
                            Some(Orientation::Vertical) => {
                                assert!(m[0] == x);
                            }
                        }
                    } else {
                        v.marker = Some([x, y]);
                    }
                    v.length += 1;
                }
            }
        }

        let vehicles = vehicles.into_iter()
            .map(|wv| Vehicle {
                marker: wv.marker.unwrap(),
                orientation: wv.orientation.unwrap(),
                length: wv.length,
            }).collect();

        Ok(Grid {
            size: w,
            cars: cars,
            trucks: trucks,
            cells: cells,
            vehicles: vehicles,
        })
    }

    pub fn get(&self, x: Size, y: Size) -> Cell {
        self.cells[self.index(x, y)]
    }

    pub fn set(&mut self, x: Size, y: Size, c: Cell) {
        let i = self.index(x, y);
        self.cells[i] = c;
    }

    pub fn index(&self, x: Size, y: Size) -> usize {
        if x >= self.size { panic!("out of bounds"); }
        x as usize + self.size as usize * y as usize
    }

    pub fn vehicle_index(&self, cell: &Cell) -> Option<usize> {
        cell.vehicle_index(self.cars)
    }

    pub fn cell_position(&self, c: usize) -> [Size; 2] {
        [c as Size % self.size, c as Size / self.size]
    }
}

impl Cell {
    fn from_str(s: &str, cars: &mut Id, trucks: &mut Id) -> Self {
        use std::cmp::max;

        let (s, r) = s.split_at(1);
        if s == "0" {
            if !r.is_empty() { warn!("extra data ignored"); }
            Cell::Empty
        } else if s == "g" {
            if !r.is_empty() { warn!("extra data ignored"); }
            Cell::RedCar
        } else if s == "c" {
            let id = r.parse().expect("could not parse car id");
            *cars = max(*cars, id);
            Cell::Car(id)
        } else if s == "t" {
            let id = r.parse().expect("could not parse truck id");
            *trucks = max(*trucks, id);
            Cell::Truck(id)
        } else {
            panic!("could not parse cell");
        }
    }

    fn vehicle_index(&self, cars: Id) -> Option<usize> {
        match self {
            &Cell::Empty => None,
            &Cell::RedCar => Some(0),
            &Cell::Car(id) => Some(id as usize),
            &Cell::Truck(id) => Some((id + cars) as usize),
        }
    }
}

#[derive(Debug)]
pub struct Solution {
    pub cost: u32,
    pub moves: Vec<Move>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Move {
    pub vehicle: usize,
    pub target: [Size; 2],
}

#[derive(Debug)]
pub enum SolveResult {
    Optimal(Solution),
    Infeasible,
    TimeLimit(Option<Solution>),
}
