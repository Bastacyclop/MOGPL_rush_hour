# Projet MOGPL - Rush Hour

Language choisi: [Rust](https://www.rust-lang.org).

## Prérequis

- Rust nightly 1.13
- Gurobi 6.5 avec les variables d'environnement appropriées
- OpenGL >= 3.2 pour lancer l'interface

Une méthode simple pour installer Rust nightly 1.13 sur Linux ou Mac:
```sh
curl -sSf https://static.rust-lang.org/rustup.sh | sh -s -- --revision=1.13.0-nightly
```

## Générer la documentation

```sh
cargo doc --no-deps -p rush_hour -p gurobi
# généré dans './target/doc/rush_hour/index.html'
```

## Compiler et Executer

La compilation risque d'être relativement longue car nous n'avons pas
fait très attention aux dépendances pour l'interface graphique.

```sh
# lancer la résolution d'un puzzle sur la console
cargo run --release --bin solve -- puzzle_path [method=gurobi [objective=RHM [N=50]]]
# lancer notre interface graphique
cargo run --release --bin gui [-- puzzle_path]
# lancer les mesures experimentales (40 * 2 * 2mn = 2h40 au pire)
cargo run --release --bin experiment
# les résultats sont disponibles dans './experiment/results'
cd experiment
gnuplot plot_cmds
```

-------------------------------------------------------------------------------

Thomas Koehler & Andrew Hatoum
